# kotlin-exposed-test

KotlinとExposedを使ったテスト

## 実行方法

0. Dockerコンテナの起動
    
    `docker-compose up -d`

0. 実行

    `docker exec kotlin-exposed ./gradlew run`
    
0. データリセット

    `docker exec kotlin-exposed-db psql exposed -U exposed -f 0_init.sql`

## テスト内容


### DSL

* [x] 参照
    * [x] 全件取得
    * [x] 主キーで取得
    * [x] ユニークキーで取得
    * [x] 複数条件で取得
    * [x] グループ
    * [x] ソート
    * [x] リミット
* [x] 結合
    * [x] INNER JOIN
    * [x] FULL JOIN
    * [x] LEFT JOIN
* [x] 追加
* [x] 更新
    * [x] 全件更新
    * [x] 主キーで更新
    * [x] 複数条件で更新
* [x] 削除
    * [x] 全件削除
    * [x] 主キーで削除
    * [x] 複数条件で削除


### DAO

* [x] 参照
    * [x] 全件取得
    * [x] 主キーで取得
    * [x] ユニークキーで取得
    * [x] 複数条件で取得
    * [x] グループ => 不可
    * [x] ソート => databaseのorderByではなく、kotlinのcollectionのソート機能
    * [x] リミット
* [ ] 結合
    * [ ] INNER JOIN
    * [ ] OUTER JOIN
    * [ ] LEFT JOIN
* [ ] 追加
* [ ] 更新
    * [ ] 全件更新
    * [ ] 主キーで更新
    * [ ] ユニークキーで更新
    * [ ] 複数条件で更新
* [ ] 削除
    * [ ] 全件削除
    * [ ] 主キーで削除
    * [ ] ユニークキーで削除
    * [ ] 複数条件で削除
