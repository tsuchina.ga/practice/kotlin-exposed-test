SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;


-- categories table
drop table if exists categories;
create table categories (
  id serial primary key,
  name varchar(256) not null,
  created_at timestamp not null default current_timestamp
);

-- categories record
insert into categories (name) values ('本'), ('食品'), ('ホビー'), ('DVD'), ('パソコン');


-- tags table
drop table if exists tags;
create table tags (
  id serial primary key,
  name varchar(256) not null,
  is_deleted boolean not null default false,
  created_at timestamp not null default current_timestamp,
  updated_at timestamp not null default current_timestamp
);

-- tags record
insert into tags (name) values ('人気'), ('ばりおもろい'), ('なかなか'), ('そこそこ'), ('うんち');


-- items table
drop table if exists items;
create table items (
  id bigserial primary key,
  category_id int not null,
  name varchar(256) not null,
  detail text not null default '',
  price decimal not null,
  quantity int not null default 0,
  is_valid boolean not null default true,
  is_deleted boolean not null default false,
  created_at timestamp not null default current_timestamp,
  updated_at timestamp not null default current_timestamp
);

-- items record
insert into items (category_id, name, price, quantity)
select id, name || '-001', 100.0, 30
from categories;

insert into items (category_id, name, price, quantity)
select id, name || '-002', 25.6, 10
from categories;

insert into items (category_id, name, price, quantity)
select id, name || '-003', 1000000, 3
from categories;


-- item_tags table
drop table if exists item_tags;
create table item_tags (
  id bigserial primary key,
  item_id bigint not null,
  tag_id int not null
);

alter table item_tags add unique (item_id, tag_id);

-- item_tags record
insert into item_tags (item_id, tag_id)
select items.id, tags.id from items, tags;

delete from item_tags where id % 7 = 0;
