package dao.model

import dao.table.Items
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass

class Item(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<Item>(Items)

    var categoryId by Items.categoryId
    var name by Items.name
    var detail by Items.detail
    var price by Items.price
    var quantity by Items.quantity
    var isValid by Items.isValid
    var isDeleted by Items.isDeleted
    var createdAt by Items.createdAt
    var updatedAt by Items.updatedAt
}
