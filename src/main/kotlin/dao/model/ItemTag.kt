package dao.model

import dao.table.ItemTags
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass

class ItemTag(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<ItemTag>(ItemTags)

    var itemId by ItemTags.itemId
    var tagId by ItemTags.tagId
}
