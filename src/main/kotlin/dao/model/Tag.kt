package dao.model

import dao.table.Tags
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class Tag(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Tag>(Tags)

    var name by Tags.name
    var isDeleted by Tags.isDeleted
    var createdAt by Tags.createdAt
    var updatedAt by Tags.updatedAt
}
