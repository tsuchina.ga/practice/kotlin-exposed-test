package dao

import dao.model.Category
import dao.model.Item
import dao.model.ItemTag
import dsl.table.ItemTags
import dsl.table.Items
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction

fun findAll() = transaction {
    addLogger(StdOutSqlLogger)

    Category.all().forEach { category -> println(category.readValues) }
}

fun findByPrimaryKey() = transaction {
    addLogger(StdOutSqlLogger)

    println(Category.findById(1)?.readValues)
    println(Category.findById(0)?.readValues)
}

fun findByUniqueKey() = transaction {
    addLogger(StdOutSqlLogger)

    ItemTag.find { ItemTags.itemId.eq(1) and ItemTags.tagId.eq(1) }.forEach { itemTag -> println(itemTag.readValues) }
    ItemTag.find { ItemTags.itemId.eq(1) and ItemTags.tagId.eq(0) }.forEach { itemTag -> println(itemTag.readValues) }
    ItemTag.find { ItemTags.itemId.eq(0) and ItemTags.tagId.eq(1) }.forEach { itemTag -> println(itemTag.readValues) }
}

fun findBy() = transaction {
    addLogger(StdOutSqlLogger)

    Item.find { Items.categoryId.eq(1) and Items.price.eq(25.6.toBigDecimal()) }.forEach { item -> println(item.readValues) }
    Item.find { Items.categoryId.inList(listOf(3, 5)) and Items.price.greaterEq(1_000_000.toBigDecimal()) }.forEach { item -> println(item.readValues) }
    Item.find { Items.name.like("%本%") }.forEach { item -> println(item.readValues) }
}

fun findAndOrder() = transaction {
    addLogger(StdOutSqlLogger)

    Category.all().sortedBy { it.id }.forEach { category -> println(category.readValues) }
    Category.all().sortedByDescending { it.id }.forEach { category -> println(category.readValues) }
    Category.all().sortedBy { it.name }.forEach { category -> println(category.readValues) }
    Category.all().sortedByDescending { it.name }.forEach { category -> println(category.readValues) }
}

fun findAndLimit() = transaction {
    addLogger(StdOutSqlLogger)

    ItemTag.all().limit(5).forEach { row -> println(row.readValues) }
    ItemTag.all().limit(5, offset = 5).forEach { row -> println(row.readValues) }
    ItemTag.all().limit(0).forEach { row -> println(row.readValues) }
}
