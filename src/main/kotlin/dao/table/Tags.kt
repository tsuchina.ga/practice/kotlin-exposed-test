package dao.table

import org.jetbrains.exposed.dao.IntIdTable
import org.joda.time.DateTime

object Tags : IntIdTable() {
    val name = varchar("name", 256)
    val isDeleted = bool("is_deleted").default(false)
    val createdAt = datetime("created_at").default(DateTime.now())
    val updatedAt = datetime("updated_at").default(DateTime.now())
}
