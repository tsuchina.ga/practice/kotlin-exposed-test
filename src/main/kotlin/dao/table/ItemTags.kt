package dao.table

import org.jetbrains.exposed.dao.LongIdTable

object ItemTags : LongIdTable("item_tags") {
    val itemId = long("item_id")
    val tagId = integer("tag_id")
}
