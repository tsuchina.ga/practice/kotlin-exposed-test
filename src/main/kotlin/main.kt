
import org.jetbrains.exposed.sql.Database

fun main(args: Array<String>) {
    // database
    Database.connect(
        url = "jdbc:postgresql://kotlin-exposed-db/exposed",
        driver = "org.postgresql.Driver",
        user = "exposed",
        password = "exposed")

    println("こんにちわーるど")

//    println("----- dsl find all -----")
//    dsl.findAll()
//    println("----- dsl find by primary key -----")
//    dsl.findByPrimaryKey()
//    println("----- dsl find by unique key -----")
//    dsl.findByUniqueKey()
//    println("----- dsl find by  -----")
//    dsl.findBy()
//    println("----- dsl find and group by  -----")
//    dsl.findAndGroup()
//    println("----- dsl find and order by  -----")
//    dsl.findAndOrder()
//    println("----- dsl find and limit  -----")
//    dsl.findAndLimit()
//
//    println("----- dsl inner join -----")
//    dsl.innerJoin()
//    println("----- dsl full join -----")
//    dsl.fullJoin()
//    println("----- dsl left join -----")
//    dsl.leftJoin()
//
//    println("----- dsl insert one -----")
//    dsl.insertOne()
//    println("----- dsl error by not null at insert  -----")
//    try { dsl.insertNotNullError() } catch (e: ExposedSQLException) { e.printStackTrace() }
//    println("----- dsl error by unique key at insert -----")
//    try { dsl.insertUniqueError() } catch (e: ExposedSQLException) { e.printStackTrace() }
//
//    println("----- dsl update all record -----")
//    dsl.updateAll()
//    println("----- dsl find at primary key and update -----")
//    dsl.updateById()
//    println("----- dsl find and update -----")
//    dsl.updateBy()
//
//    println("----- dsl delete all record -----")
//    dsl.deleteAll()
//    println("----- dsl find at primary key and delete -----")
//    dsl.deleteById()
//    println("----- dsl find and delete -----")
//    dsl.deleteBy()

    println("----- dao find all -----")
    dao.findAll()
    println("----- dao find by primary key -----")
    dao.findByPrimaryKey()
    println("----- dao find by unique key -----")
    dao.findByUniqueKey()
    println("----- dao find by -----")
    dao.findBy()
    println("----- dao find and order by -----")
    dao.findAndOrder()
    println("----- dao find and limit -----")
    dao.findAndLimit()
}
