package dsl

import dsl.table.Categories
import dsl.table.ItemTags
import dsl.table.Items
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.count
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.sum
import org.jetbrains.exposed.sql.transactions.transaction

fun findAll() = transaction {
    addLogger(StdOutSqlLogger)

    Categories.selectAll().forEach { row -> println(row) }
}

fun findByPrimaryKey() = transaction {
    addLogger(StdOutSqlLogger)

    Categories.select { Categories.id.eq(1) }.forEach { row -> println(row) }
    Categories.select { Categories.id.eq(0) }.forEach { row -> println(row) }
}

fun findByUniqueKey() = transaction {
    addLogger(StdOutSqlLogger)

    ItemTags.select { ItemTags.itemId.eq(1) and ItemTags.tagId.eq(1) }.forEach { row -> println(row) }
    ItemTags.select { ItemTags.itemId.eq(1) and ItemTags.tagId.eq(0) }.forEach { row -> println(row) }
    ItemTags.select { ItemTags.itemId.eq(0) and ItemTags.tagId.eq(1) }.forEach { row -> println(row) }
}

fun findBy() = transaction {
    addLogger(StdOutSqlLogger)

    Items.select { Items.categoryId.eq(1) and Items.price.eq(25.6.toBigDecimal()) }.forEach { row -> println(row) }
    Items.select { Items.categoryId.inList(listOf(3, 5)) and Items.price.greaterEq(1_000_000.toBigDecimal()) }.forEach { row -> println(row) }
    Items.select { Items.name.like("%本%") }.forEach { row -> println(row) }
}

fun findAndGroup() = transaction {
    addLogger(StdOutSqlLogger)

    ItemTags.slice(ItemTags.itemId, ItemTags.id.count()).selectAll().groupBy(ItemTags.itemId).forEach { row -> println(row) }
    Items.slice(Items.id, Items.quantity.sum()).selectAll().groupBy(Items.id).forEach { row -> println(row) }
}

fun findAndOrder() = transaction {
    addLogger(StdOutSqlLogger)

    Categories.selectAll().orderBy(Categories.id to true).forEach { row -> println(row) }
    Categories.selectAll().orderBy(Categories.id to false).forEach { row -> println(row) }
    Categories.selectAll().orderBy(Categories.name to true).forEach { row -> println(row) }
    Categories.selectAll().orderBy(Categories.name to false).forEach { row -> println(row) }
}

fun findAndLimit() = transaction {
    addLogger(StdOutSqlLogger)

    ItemTags.selectAll().orderBy(ItemTags.id to true).limit(5).forEach { row -> println(row) }
    ItemTags.selectAll().orderBy(ItemTags.id to true).limit(5, offset = 5).forEach { row -> println(row) }
    ItemTags.selectAll().orderBy(ItemTags.id to true).limit(0).forEach { row -> println(row) }
}
