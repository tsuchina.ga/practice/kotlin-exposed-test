package dsl

import dsl.table.Tags
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.or
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import org.joda.time.DateTime

fun updateAll() = transaction {
    addLogger(StdOutSqlLogger)

    Tags.slice(Tags.id, Tags.updatedAt).selectAll().forEach { row -> println(row) }

    Tags.update { it[updatedAt] = DateTime.now() }

    Tags.slice(Tags.id, Tags.updatedAt).selectAll().forEach { row -> println(row) }
}

fun updateById() = transaction {
    addLogger(StdOutSqlLogger)

    Tags.slice(Tags.id, Tags.name).select { Tags.id eq 1 }.forEach { row -> println(row) }

    Tags.update({ Tags.id eq 1 }) { it[name] = "最強" }

    Tags.slice(Tags.id, Tags.name).select { Tags.id eq 1 }.forEach { row -> println(row) }
}

fun updateBy() = transaction {
    addLogger(StdOutSqlLogger)

    Tags.slice(Tags.id, Tags.name).select { (Tags.id inList listOf(1, 3, 3)) or (Tags.name eq "うんち") }.forEach { row -> println(row) }

    Tags.update({ (Tags.id inList listOf(1, 3, 3)) or (Tags.name eq "うんち") }) { it[name] = "最強" }

    Tags.slice(Tags.id, Tags.name).select { (Tags.id inList listOf(1, 3, 3)) or (Tags.name eq "うんち") }.forEach { row -> println(row) }
}
