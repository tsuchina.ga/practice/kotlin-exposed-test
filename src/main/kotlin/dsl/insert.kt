package dsl

import dsl.table.Categories
import dsl.table.ItemTags
import dsl.table.Items
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.count
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

fun insertOne() = transaction {
    addLogger(StdOutSqlLogger)

    Categories.slice(Categories.id.count()).selectAll().forEach { row -> println(row) }

    Categories.insert { category ->
        category[name] = "新しいカテゴリ"
    }

    Categories.slice(Categories.id.count()).selectAll().forEach { row -> println(row) }
}

fun insertNotNullError() = transaction {
    addLogger(StdOutSqlLogger)

    Items.slice(Items.id.count()).selectAll().forEach { row -> println(row) }

    Items.insert { }

    Items.slice(Items.id.count()).selectAll().forEach { row -> println(row) }
}

fun insertUniqueError() = transaction {
    addLogger(StdOutSqlLogger)

    ItemTags.slice(ItemTags.id.count()).selectAll().forEach { row -> println(row) }

    ItemTags.insert { itemTag ->
        itemTag[itemId] = 1
        itemTag[tagId] = 1
    }

    ItemTags.slice(ItemTags.id.count()).selectAll().forEach { row -> println(row) }
}
