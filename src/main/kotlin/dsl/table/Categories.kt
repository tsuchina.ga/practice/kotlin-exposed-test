package dsl.table

import org.jetbrains.exposed.dao.IntIdTable
import org.joda.time.DateTime

object Categories : IntIdTable() {
    val name = varchar("name", 256)
    val createdAt = datetime("created_at").default(DateTime.now())
}
