package dsl.table

import org.jetbrains.exposed.dao.LongIdTable
import org.joda.time.DateTime

object Items : LongIdTable() {
    val categoryId = integer("category_id")
    val name = varchar("name", 256)
    val detail = text("detail").default("")
    val price = decimal("price", 10, 6)
    val quantity = integer("quantity")
    val isValid = bool("is_valid").default(true)
    val isDeleted = bool("is_deleted").default(false)
    val createdAt = datetime("created_at").default(DateTime.now())
    val updatedAt = datetime("updated_at").default(DateTime.now())
}
