package dsl

import dsl.table.Categories
import dsl.table.Items
import org.jetbrains.exposed.sql.JoinType
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

fun innerJoin() = transaction {
    addLogger(StdOutSqlLogger)

    Items.join(Categories, JoinType.INNER, additionalConstraint = { Items.categoryId eq Categories.id })
        .slice(Items.id, Items.categoryId, Categories.name).selectAll().orderBy(Items.id).forEach { row -> println(row) }
}

fun fullJoin() = transaction {
    addLogger(StdOutSqlLogger)

    Categories.insert { it[name] = "関係しないカテゴリ" }

    Items.join(Categories, JoinType.FULL, additionalConstraint = { Items.categoryId eq Categories.id })
        .slice(Items.id, Items.categoryId, Categories.name).selectAll().orderBy(Items.id).forEach { row -> println(row) }
}

fun leftJoin() = transaction {
    addLogger(StdOutSqlLogger)

    Items.insert {
        it[categoryId] = 0
        it[name] = "存在しないカテゴリの商品"
        it[price] = 0.toBigDecimal()
        it[quantity] = 0
    }

    Items.join(Categories, JoinType.LEFT, additionalConstraint = { Items.categoryId eq Categories.id })
        .slice(Items.id, Items.categoryId, Categories.name).selectAll().orderBy(Items.id).forEach { row -> println(row) }
}
