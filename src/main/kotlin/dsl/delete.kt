package dsl

import dsl.table.Categories
import dsl.table.Items
import dsl.table.Tags
import org.jetbrains.exposed.sql.StdOutSqlLogger
import org.jetbrains.exposed.sql.addLogger
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction

fun deleteAll() = transaction {
    addLogger(StdOutSqlLogger)

    Tags.selectAll().forEach { row -> println(row) }

    Tags.deleteAll()

    Tags.selectAll().forEach { row -> println(row) }
}

fun deleteById() = transaction {
    addLogger(StdOutSqlLogger)

    Categories.select { Categories.id eq 1 }.forEach { row -> println(row) }

    Categories.deleteWhere { Categories.id eq 1 }

    Categories.select { Categories.id eq 1 }.forEach { row -> println(row) }
}

fun deleteBy() = transaction {
    addLogger(StdOutSqlLogger)

    Items.select { (Items.id inList listOf(1L, 2L, 3L)) and (Items.categoryId eq 1) }.forEach { row -> println(row) }

    Items.deleteWhere { (Items.id inList listOf(1L, 2L, 3L)) and (Items.categoryId eq 1) }

    Items.select { (Items.id inList listOf(1L, 2L, 3L)) and (Items.categoryId eq 1) }.forEach { row -> println(row) }
}
